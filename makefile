.PHONY: default
default: main
include arch.makefile
printStats.o: printStats.c a2.h
read_process.o: read_process.c a2.h colors.h
main.o: main.c a2.h read_process.h printStats.h
colors.o: colors.c colors.h
main: ./printStats.o ./read_process.o ./main.o ./colors.o
clean:
	find . -type f -name '*.o' | xargs -r -d \\n rm -vf main
run: main
	./main ${ARGS}
valrun: main
	valgrind ./main ${ARGS}
