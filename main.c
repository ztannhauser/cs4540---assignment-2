#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "a2.h"
#include "read_process.h"
#include "printStats.h"

int compare(struct process_struct* a,
	struct process_struct* b, struct os_struct* os)
{
	return
				(a->priority + a->wait / os->wait)
			-
				(b->priority + b->wait / os->wait)
		?:
				b->cpuTotal / os->quantum
			-
				a->cpuTotal / os->quantum
	;
}

int main(int args_n, const char** args)
{
	if(args_n == 2)
	{
		struct os_struct os = {.quantum = 70, .wait = 30};
		const char* inputfilepath = args[1];
		FILE* inputfile = fopen(inputfilepath, "r");
		struct process_struct ready[P_COUNT], io[P_COUNT];
		int ready_n = 0, io_n = 0, left_to_be_added_n = P_COUNT;
		while(ready_n || io_n || left_to_be_added_n)
		{
			if(left_to_be_added_n)
			{
				read_process(inputfile, ready + ready_n++);
				left_to_be_added_n--;
			}
			if(ready_n)
			{
				qsort_r(ready, ready_n, sizeof(struct process_struct),
					compare, &os);
				int nm1 = ready_n - 1;
				struct process_struct* has_cpu = ready + nm1;
				has_cpu->cpuTotal++;
				has_cpu->wait = 0;
				if(has_cpu->cpuTotal == has_cpu->cpu)
				{
	/*				printf("moved process from cpu to io\n");*/
					memcpy(io + io_n++, ready + nm1,
						sizeof(struct process_struct));
					ready_n--;
				}
				else if(has_cpu->cpuTotal >= has_cpu->runTime)
				{
	/*				printf("process has ended\n");*/
					ready_n--;
				}
				while(nm1--)
				{
					(ready + nm1)->wait++;
					(ready + nm1)->waitSum++;
				}
			}
			for(int io_src = 0, io_dest = 0, old_n = io_n;
				io_src < old_n;io_src++)
			{
				struct process_struct* src_ele = io + io_src;
				if((src_ele)->curIo++ < (src_ele)->io)
				{
/*					printf("aging io process %i\n", io_src);*/
					if(io_src != io_dest)
					{
						memcpy(io + io_dest, src_ele,
							sizeof(struct process_struct));
					}
					io_dest++;
				}
				else
				{
/*					printf("moving io process back into ready queue\n");*/
					memcpy(ready + ready_n++, src_ele,
						sizeof(struct process_struct));
					io_n--;
				}
			}
			#if 1
			{
				printf("\033[" "2J");
				for(int i = 0;i < ready_n;i++)
				{
					struct process_struct* ele = ready + i;
					printf("%s", ele->colorstring);
					printf("%3i: priority: %3i, cpu: %3i, io: %3i, runTime: %i, "
					"cpuTotal: %i, wait: %i, waitSum: %i\n",
						i, ele->priority, ele->cpu, ele->io, ele->runTime,
						ele->cpuTotal, ele->wait, ele->waitSum);
					printf("\033[" "0" "m");
				}
				printf("\n\n");
				for(int i = 0;i < io_n;i++)
				{
					struct process_struct* ele = io + i;
					printf("%s", ele->colorstring);
					printf("%3i: priority: %3i, cpu: %3i, io: %3i, runTime: %i, "
					"cpuTotal: %i, wait: %i, curIo: %i\n",
						i, ele->priority, ele->cpu, ele->io, ele->runTime,
						ele->cpuTotal, ele->wait, ele->curIo);
					printf("\033[" "0" "m");
				}
				usleep(100 * 1000);
			}
			#endif
		}
		fclose(inputfile);
/*		printStats(ready, os);*/
	}
	else
	{
		printf("usage: %s <input file>\n", args[0]);
	}
	return 0;
}











