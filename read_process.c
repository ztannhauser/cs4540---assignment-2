#include <stdio.h>

#include "a2.h"
#include "colors.h"

void read_process(FILE* in, struct process_struct* out)
{
	
	fscanf(in, "%u %u %u %u", &(out->priority),
		&(out->cpu), &(out->io), &(out->runTime));
	sprintf(out->colorstring, "\e[38;2;%u;%u;%um",
		colors[out->priority].r,
		colors[out->priority].g,
		colors[out->priority].b
	);
	out->wait = 0;
	out->curIo = 0;
	out->waitSum = 0;
	out->cpuTotal = 0;
}
